const express       = require('express');
const path          = require('path');
const serveStatic   = require('serve-static');

const app = express();

const absolutePath = path.join(__dirname, 'dist');

app.use(serveStatic(absolutePath));

const port = process.env.PORT || 5000;

// catch all routes and redirect to the index file
app.get('*', (req, res) => {
  res.sendFile(`${__dirname}/dist/index.html`);
});

app.listen(port);

console.log(`server started on : ${port}`);
