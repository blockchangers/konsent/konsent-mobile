module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    //"airbnb-base",
    'plugin:vue/essential',
  ],
  rules: {
    'no-multi-spaces': ['error', { exceptions: { VariableDeclarator: true } }],
    'no-bitwise': ['error', { allow: ['~'] }],
    'max-len': ['error', { code: 160 }],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'comma-dangle': 'off',
    'no-unused-vars': 'off',
    'arrow-body-style': 'off',
    'quotes': 'off',
    'import/prefer-default-export': 'off'
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};