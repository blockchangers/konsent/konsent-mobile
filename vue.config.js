module.exports = {
  devServer: {
    https: true,
    proxy: {
      '/api/v1/organizations': {
        target: 'http://localhost:8082',
      },
      '/api/v1/claims': {
        target: 'http://localhost:8083',
      },
    },
    watchOptions: {
      poll: true
    },
  },
};
