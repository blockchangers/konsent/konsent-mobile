import babelPolyfill from 'babel-polyfill';
import 'vant/lib/index.css';
import './registerServiceWorker';
import { sync } from 'vuex-router-sync';
import * as Vant from 'vant';
import Vue from 'vue';
import VueAnalytics from 'vue-analytics';
import VueUniqIds from 'vue-uniq-ids';
import enUS from 'vant/lib/locale/lang/en-US';
import router from './router';
import store from './store';

import App from './App.vue';

Vant.Locale.use('en-US', enUS);

Vue.use(Vant);
Vue.use(VueUniqIds);

sync(store, router, { moduleName: 'Router' });

const isProd = process.env.NODE_ENV === 'production';

Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  router,
  debug: {
    enabled: !isProd,
    sendHitTask: isProd,
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
