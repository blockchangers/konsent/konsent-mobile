const names = {
  HOME: 'home',
  ORGANIZATIONS: 'organizations',
  ORGANIZATION: 'organization',
  INSTALLATION: 'installation',
  PROFILE: 'profile',
  SETTINGS: 'settings',
  WALLET: 'wallet',
  RESTORE: 'restore',
  DASHBOARD: 'dashboard',
};

export default names;
