/* eslint-disable */

export const logKeysWithSizeInLocalStorage = () => {
  var total = 0;
  for(var x in localStorage) {
    var amount = (localStorage[x].length * 2) / 1024 / 1024;
    total += amount;
    console.log( x + " = " + amount.toFixed(2) + " MB");
  }
  console.log( "Total: " + total.toFixed(2) + " MB");
};

export const clear = (cacheName) => {
  window.localStorage.clear();
  return caches.delete(cacheName);
};

const getStorageEstimates = () => {
  return new Promise((resolve, reject) => {
    if ('storage' in navigator && 'estimate' in navigator.storage) {
      navigator.storage.estimate()
      .then(estimate => {
        resolve((estimate.usage / estimate.quota).toFixed(2));
      })
      .catch(reject);
    } else {
      reject(new Error('navigator.storage not supported'));
    }
  });
};
