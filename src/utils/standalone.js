// Detects if device is on iOS
const isIos = () => /iphone|ipad|ipod/.test(window.navigator.userAgent.toLowerCase());

// Detects if device is in standalone mode
const isInStandaloneMode            = () => ('standalone' in window.navigator) && (window.navigator.standalone);

const isSafariStandAlone            = () => isIos() && isInStandaloneMode();
const isChromeStandAlone            = () => window.matchMedia('(display-mode: standalone)').matches;

const isStandAloneMode              = () => isSafariStandAlone() || isChromeStandAlone();

// display it only in safari and not in standalone mode (when the app is already added to home screen
// const shouldDisplayInstallDialog    = isIos() && !isInStandaloneMode();

export default isStandAloneMode;
