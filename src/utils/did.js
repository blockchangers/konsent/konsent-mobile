const EthrDID = require('ethr-did');
const didJWT = require('did-jwt');

require('ethr-did-resolver')();

export const create = (web3, wallet) => new EthrDID({
  address: wallet.address,
  privateKey: wallet.privateKey.substring(2),
  provider: web3.currentProvider,
});

export async function verify(claim) {
  return didJWT.verifyJWT(claim);
}

export async function sign(did, audience) {
  return did.signJWT({ audience });
}

export function decode(did) {
  return didJWT.decodeJWT(did);
}
