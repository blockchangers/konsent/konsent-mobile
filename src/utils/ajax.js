const request = require('request');

const put = (url, jwt) => new Promise((resolve, reject) => {
  request({
    url,
    method: 'PUT',
    json: { jwt },
  }, (error, response, body) => {
    if (error) {
      reject(error);
    } else {
      resolve(body);
    }
  });
});


const validateToken = (url, { token, phone }) => new Promise((resolve, reject) => {
  return fetch(url, {
    method: 'POST',
    body: JSON.stringify({token, phone}),
    headers: new Headers({
      'Content-Type': 'application/json'
    }),
  })
  .then(response => response.json())
  .then(resolve)
  .catch(error => {
    reject(error);
  });
});

const post = (url = '', data = {}) => fetch(url, {
  method: 'POST',
  mode: 'cors',
  cache: 'no-cache',
  redirect: 'follow',
  referrer: 'no-referrer',
  body: { jwt: data },
}).then(response => response.json());

const queryOrgs = (url, { bpvJwt, userJwt }) => new Promise((resolve, reject) => {
  request({
    url,
    method: 'POST',
    json: { bpvJwt, userJwt },
  }, (error, response, body) => {
    if (error) {
      reject(error);
    } else {
      resolve(body);
    }
  });
});

const get = (url = '') => fetch(url).then(response => response.json());

export {
  validateToken,
  put,
  post,
  get,
  queryOrgs,
};
