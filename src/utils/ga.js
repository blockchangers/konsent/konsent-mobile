import { event } from 'vue-analytics';

const clickEvent = (data) => {
  event('user-click', 'increase', 'counter', data);
};

export default clickEvent;
