/* eslint-disable */

import { ethers } from 'ethers';

export const privateKeyFromSeed = (seed) => {
  const { privateKey } = ethers.Wallet.fromMnemonic(seed);
  return privateKey;
};

export const generateRandomSeed = () => {
  const { mnemonic } = ethers.Wallet.createRandom();
  return mnemonic;
};

export const create = seed => ethers.Wallet.fromMnemonic(seed);

export const createEncrypted = (mnemonic, password, cb) => {
  const wallet = ethers.Wallet.fromMnemonic(mnemonic);
  const options = {
    scrypt: {
      N: (1 << 12), // lower number means less secure but faster to generate
    },
  };
  return wallet.encrypt(password, options, (progress) => {
    if (cb) { cb(progress); }
  });
};

export const load = async (wallet, password) => {
  try {
    const loadedWallet = await ethers.Wallet.fromEncryptedJson(wallet, password);
    return Promise.resolve(loadedWallet);
  } catch (error) {
    return Promise.reject(error);
  }
};
