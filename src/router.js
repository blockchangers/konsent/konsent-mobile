import { CREATE_DID, basePath as walletBasePath } from '@/components/wallet/actions';
import { OrganizationClaims, OrganizationList } from '@/components/organization/';
import { Restore, Wallet } from '@/components/wallet/';
import Home from '@/components/home/';
import Installation from '@/components/installation/';
import Router from 'vue-router';
import Vue from 'vue';
import names from './names';
import store from './store';

const Dashboard = () => import(/* webpackChunkName: "group-foo" */ '@/components/dashboard/index');

Vue.use(Router);

const isWalletNotReady = state => state.getters['Wallet/isWalletNotReady'];

const router = new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      name: names.HOME,
      component: Home,
      meta: {
        title: 'Home',
      },
    },
    {
      path: '/wallet',
      name: names.WALLET,
      component: Wallet,
    },
    {
      path: '/restore',
      name: names.Restore,
      component: Restore,
    },
    {
      path: '/dashboard',
      name: names.DASHBOARD,
      component: Dashboard,
      beforeEnter: (to, from, next) => {
        return (isWalletNotReady(store) ? next('/restore') : next());
      },
    },
    {
      path: '/organizations',
      name: names.ORGANIZATIONS,
      component: OrganizationList,
      beforeEnter: (to, from, next) => (isWalletNotReady(store) ? next('/restore') : next()),
    },
    {
      path: '/organizations/:id',
      name: names.ORGANIZATION,
      component: OrganizationClaims,
      beforeEnter: (to, from, next) => (isWalletNotReady(store) ? next('/restore') : next()),
      props: route => ({ id: route.params.id }),
    },
    {
      path: '/installation',
      name: names.INSTALLATION,
      component: Installation,
      props: route => ({
        uuidFromRouter: route.params.uuid,
        didFromRouter: route.params.did,
        urlFromRouter: route.params.url,
      }),
    },
    {
      path: '/installation/:uuid/:did/:url',
      name: names.INSTALLATION,
      component: Installation,
      props: route => ({
        uuidFromRouter: route.params.uuid,
        didFromRouter: route.params.did,
        urlFromRouter: route.params.url,
      }),
    },
  ],
});

router.beforeEach((to, from, next) => {
  const title = to.meta && to.meta.title;
  if (title) {
    document.title = title;
  }
  next();
});

export default router;
