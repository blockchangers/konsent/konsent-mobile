import Home from '@/components/home/store';
import Installation from '@/components/installation/store';
import Organization from '@/components/organization/store';
import Services from '@/components/services/store';
import Otp from '@/components/otp/store';
import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import Wallet from '@/components/wallet/store';
import createLogger from 'vuex/dist/logger';

const vuexPersist = new VuexPersist({
  key: 'konsent-mobile',
  storage: localStorage,
});

const debug = process.env.NODE_ENV !== 'production';

const plugins = debug ? [createLogger(), vuexPersist.plugin] : [vuexPersist.plugin];

Vue.use(Vuex);

export default new Vuex.Store({
  plugins,
  strict: debug,
  modules: {
    Installation,
    Home,
    Wallet,
    Organization,
    Otp,
    Services
  },
});
