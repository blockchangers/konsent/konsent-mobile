export const FETCH_ORGS_INIT              = 'FETCH_ORGS_INIT';
export const FETCH_ORG_CLAIMS             = 'FETCH_ORG_CLAIMS';
export const SET_SELECTED_ORG             = 'SET_SELECTED_ORG';
export const SET_SELECTED_ORG_FROM_URL    = 'SET_SELECTED_ORG_FROM_URL';
export const SET_NEW_CLAIMS               = 'SET_NEW_CLAIMS';
export const QUERY_FOR_MEMBERSHIP         = 'QUERY_FOR_MEMBERSHIP';
export const basePath                     = type => `Organization/${type}`;
