import OrganizationList from './OrganizationList.vue';
import OrganizationClaims from './OrganizationClaims.vue';

export {
  OrganizationClaims,
  OrganizationList,
};
