export const FETCH_ORGS_PENDING = 'FETCH_ORGS_PENDING';
export const FETCH_ORGS_FAILURE = 'FETCH_ORGS_FAILURE';
export const FETCH_ORGS_SUCCESS = 'FETCH_ORGS_SUCCESS';
export const FETCH_FILTERED_ORGS_SUCCESS = 'FETCH_FILTERED_ORGS_SUCCESS';
export const SET_SELECTED_ORG_SUCCESS = 'SET_SELECTED_ORG_SUCCESS';
export const COMMIT_ORG_CLAIMS = 'COMMIT_ORG_CLAIMS';
