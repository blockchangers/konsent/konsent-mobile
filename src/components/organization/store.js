/* eslint-disable */
const series  = require('async/series')
import { get, put, queryOrgs } from '../../utils/ajax';

import { FETCH_ORGS_FAILURE, FETCH_ORGS_PENDING, FETCH_ORGS_SUCCESS, SET_SELECTED_ORG_SUCCESS, COMMIT_ORG_CLAIMS, FETCH_FILTERED_ORGS_SUCCESS } from './mutations';
import { FETCH_ORGS_INIT, FETCH_ORG_CLAIMS, SET_SELECTED_ORG, SET_SELECTED_ORG_FROM_URL, SET_NEW_CLAIMS, QUERY_FOR_MEMBERSHIP } from './actions';
import { MARK_AS_INITIAL_CLAIMS_FETCHED, basePath as installationBasePath } from '../installation/actions';


const ORGANIZATION_REGISTRY_URL = process.env.VUE_APP_ORGANIZATION_REGISTRY_URL;

const namespaced = true;

const state = {
  user: {
    organizations: [],
    filteredOrganizations: [],
    loading: false,
    error: null,
    claims: [],
    selectedOrganization: null,
  }
};

const getters = {
  organizations           : state => state.user.organizations,
  filteredOrganizations   : state => state.user.filteredOrganizations,
  getOrganizationById     : (state) => (did) => state.user.organizations[did],
  getLoadingStatus        : state => state.user.loading,
  getErrorStatus          : state => state.user.error,
  getSelectedOrganization : state => state.user.selectedOrganization,
  claims                  : state => state.user.claims
};

const actions = {
  [FETCH_ORGS_INIT]: async ({ commit, dispatch }) => {
    try {
      commit(FETCH_ORGS_PENDING);
      const organizations = await get(ORGANIZATION_REGISTRY_URL);
      commit(FETCH_ORGS_SUCCESS, organizations);
      dispatch(QUERY_FOR_MEMBERSHIP, {organizations});
    } catch (error) {
      commit(FETCH_ORGS_FAILURE, error.stack);
    }
  },
  [SET_SELECTED_ORG_FROM_URL]: async({ dispatch, state }, did) => {
    if(!getters.getSelectedOrganization) {
      const organization = getters.getOrganizationById(state)(did);
      dispatch(SET_SELECTED_ORG, {organization})
    }
  },
  [SET_SELECTED_ORG]: async ({ commit }, { organization }) => {
    commit(SET_SELECTED_ORG_SUCCESS, organization);
  },
  [QUERY_FOR_MEMBERSHIP]: async ({ commit, rootGetters, state }, { organizations }) => {

    const didFromWallet = rootGetters['Wallet/did'];
    const phoneFromWallet = rootGetters['Wallet/phone'];
    const userJwt = await didFromWallet.signJWT({audience: didFromWallet.did, claims: [{ key: 'phone', value: phoneFromWallet }]});
    const bpvJwt = rootGetters['Otp/jwt'];

    if(bpvJwt) {
      try {
        const tasks = organizations.map((organization, index) => {
          return function(cb) {
            const promise = queryOrgs(organization.callbackUrl, {bpvJwt, userJwt});
            promise.then(result => {
              if(result && result.error) {
                cb(null, {status: 'failure', index, error: result.error});
              } else {
                cb(null, {status: 'success', index, result});
              }
            });
            promise.catch(error => {
              console.log("failure: ", error);
              cb(null, {status: 'failure', index, error});
            });
          }
        });

        //TODO: refactor
        series(tasks, (err, results) => {
          console.log("Organizations: ", results);
          if(err) { console.log(err); };
          let filteredOrganizations = [];

          for(let i = 0; i < results.length; i++) {
            if(results[i].status === 'success') {
              filteredOrganizations.push(organizations[i])
            }
          }
          commit(FETCH_FILTERED_ORGS_SUCCESS, filteredOrganizations);
        });
      } catch (error) {
        console.error(error);
      }
    }
  },
  [FETCH_ORG_CLAIMS]: async ({ commit , dispatch, rootGetters, state }, { userDid }) => {
    const userUuid            = rootGetters['Installation/uuid'];
    const installationStatus  = rootGetters['Installation/status'];
    const userWallet          = rootGetters['Wallet/wallet'];
    const didFromWallet       = rootGetters['Wallet/did'];
    const signedOtpJwt        = rootGetters['Otp/jwt'];
    const {callbackUrl, did}  = getters.getSelectedOrganization(state);


    if(signedOtpJwt) {
      const url = `${callbackUrl}?jwt=${signedOtpJwt}`;
      try {
        const claims = await get(url);
        commit(COMMIT_ORG_CLAIMS, claims);
      } catch (error) {
        console.error(error);
      }
    }
    // console.log("claims already fetched once");
    // const signedUserJwt = await didFromWallet.signJWT({audience: did});
    // console.log("signed jwt: ", signedUserJwt);
    // const url = `${callbackUrl}?jwt=${signedUserJwt}`;
    // try {
    //   const claims = await get(url);
    //   commit(COMMIT_ORG_CLAIMS, claims);
    // } catch (error) {
    //   console.error(error);
    // }

    // else {
    //   console.log("fetching for the first time");
    //   const url = `${callbackUrl}/${userUu0id}/${userDid}`;
    //   try {
    //     const claims = await get(url);
    //     commit(COMMIT_ORG_CLAIMS, claims);
    //     dispatch(installationBasePath(MARK_AS_INITIAL_CLAIMS_FETCHED), {state: 'STATUS_CLAIMS_FETCHED' }, {root:true});
    //   } catch (error) {
    //     console.log(error);
    //   }
    // }
  },
  [SET_NEW_CLAIMS]: async ({commit , dispatch, rootGetters, state}, { phone , email }) => {
    const {callbackUrl, did}  = getters.getSelectedOrganization(state);
    const didFromWallet = rootGetters['Wallet/did'];

    const phoneJwt = await didFromWallet.signJWT({did, claims: [{ key: 'phone', value: phone }]});
    const emailJwt = await didFromWallet.signJWT({did, claims: [{ key: 'email', value: email }]});

    await put(callbackUrl, phoneJwt);
    await put(callbackUrl, emailJwt);

    dispatch(FETCH_ORG_CLAIMS, {userDid: didFromWallet.did});
  }
};

const mutations = {
  [SET_SELECTED_ORG_SUCCESS](state, organization) {
    state.user = {
      ...state.user,
      selectedOrganization : organization
    }
  },
  [FETCH_FILTERED_ORGS_SUCCESS](state, filteredOrganizations) {
    state.user = {
      ...state.user,
      filteredOrganizations,
      loading: false,
      error: null,
    };
  },
  [FETCH_ORGS_SUCCESS](state, organizations) {
    state.user = {
      ...state.user,
      organizations,
      loading: false,
      error: null,
    };
  },
  [FETCH_ORGS_PENDING](state) {
    state.user = {
      ...state.user,
      loading: false,
      filteredOrganizations: []
    };
  },
  [FETCH_ORGS_FAILURE](state, error) {
    state.user = {
      ...state.user,
      loading: false,
      organizations: [],
      error,
    };
  },
  [COMMIT_ORG_CLAIMS](state, claims) {
    state.user = {
      ...state.user,
      claims: claims
    }
  }
};

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations,
};
