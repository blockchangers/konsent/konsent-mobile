/* eslint-disable */
const series = require('async/series')

import { GET_AVAILABLE_SERVICES, INIT, SET_NEW_CLAIMS, GET_SERVICE_DID_FROM_NAME, SAVE_SERVICE_DID } from './actions';
import { SET_ALL_AVAILABLE_SERVICES } from './mutations';
import * as didUtil from '../../utils/did';

const ORGANIZATION_REGISTRY_URL = process.env.VUE_APP_ORGANIZATION_REGISTRY_URL;

const namespaced = true;


const state = {
  services: {}
};

const getters = {
  services: state => state.services,
};

const actions = {
  [INIT]: ({ dispatch }) => {
    // Bootstraps this store. Only init when needed, like whene rendering service list
    dispatch(GET_AVAILABLE_SERVICES)
  },
  [GET_AVAILABLE_SERVICES]: async ({ commit }, payload) => {
    // this is just a mock function to that sets a list of serivices. Each with key => object
    try {
      let services = {
        "facebook": {
          active: true,
          name: "Facebook"
        },
      }
      commit(SET_ALL_AVAILABLE_SERVICES, { services })
      return services
    } catch (error) {
      console.log(error)
    }
  },
  [SET_NEW_CLAIMS]: async ({ commit, dispatch, rootGetters, state }, { serviceName, claims }) => {
    // get the current did object from localstorage
    let did = dispatch(GET_SERVICE_DID_FROM_NAME, { serviceName: serviceName })

    // Get walletDid object
    const didFromWallet = rootGetters['Wallet/did'];

    //go through each claims sent inn and create a JWT with for it.

    did = await didFromWallet.signJWT({ did, claims: claims });

    console.log("Your did for " + serviceName + " is updated to: ", did);

    dispatch(SAVE_SERVICE_DID, { serviceName: serviceName, did: did })

  },
  [GET_SERVICE_DID_FROM_NAME]: ({ }, { serviceName }) => {
    const currentDidFromLocalStorage = JSON.parse(localStorage.getItem([serviceName] + '-did'))
    let did
    if (currentDidFromLocalStorage === null) {
      // if not object found locally, just init with empty props
      return {
        user: {},
        service: {}
      }
    } else {
      did = currentDidFromLocalStorage
      did = didUtil.decode(did)
    }
    if (did.hasOwnProperty('payload') && did.payload.hasOwnProperty('claims')) {
      console.log("returning ", did.payload.claims)
      return did.payload.claims
    }
    console.log("Something is wrog in GET_SERVICE_DID_FROM_NAME")
    return false
  },
  [SAVE_SERVICE_DID]: ({ }, { serviceName, did }) => {
    // Save this service DID to localstorage
    localStorage.setItem([serviceName] + '-did', JSON.stringify(did))
  }
};

const mutations = {
  [SET_ALL_AVAILABLE_SERVICES]: (state, { services }) => {
    state.services = services
  }
}
export default {
  namespaced,
  state,
  getters,
  actions,
  mutations,
};
