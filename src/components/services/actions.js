export const GET_AVAILABLE_SERVICES = 'GET_AVAILABLE_SERVICES';
export const SET_NEW_CLAIMS = 'SET_NEW_CLAIMS';
export const GET_SERVICE_DID_FROM_NAME = 'GET_SERVICE_DID_FROM_NAME';
export const SAVE_SERVICE_DID = 'SAVE_SERVICE_DID';
export const INIT = 'INIT';
export const basePath = type => `Services/${type}`;