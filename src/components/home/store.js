/* eslint-disable */
import * as wallet from '../../utils/wallet';

import { MARK_AS_INSTALLED, basePath as installationBasePath } from '../installation/actions';
import { CREATE_WALLET, basePath as walletBasePath } from '../wallet/actions';
import { TRIGGER_WALLET_GENERATION } from './actions';

const namespaced = true;

const state = {
  user: {
  }
};

const getters = {

};

const actions = {
  [TRIGGER_WALLET_GENERATION]: async ({ commit, dispatch }, { password }) => {
    return new Promise((resolve, reject) => {
      try {
        const seed = wallet.generateRandomSeed();
        dispatch(walletBasePath(CREATE_WALLET), { seed, password },{ root: true });
        //dispatch(installationBasePath(MARK_AS_INSTALLED), null, { root: true });
        resolve(seed);
      } catch (error) {
        reject(error);
      }
    });
  },
}

const mutations = {
};

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
};
