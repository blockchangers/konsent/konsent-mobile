export const MARK_AS_OPENED     = 'MARK_AS_OPENED';
export const MARK_AS_INSTALLED  = 'MARK_AS_INSTALLED';
export const MARK_AS_INITIAL_CLAIMS_FETCHED = 'MARK_AS_INITIAL_CLAIMS_FETCHED';
export const basePath           = type => `Installation/${type}`;
