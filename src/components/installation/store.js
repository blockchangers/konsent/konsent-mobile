/* eslint-disable */
import { MARK_AS_OPENED, MARK_AS_INSTALLED, MARK_AS_INITIAL_CLAIMS_FETCHED } from './actions';
import { STORAGE_PERSIST_SUCCESS, STORAGE_PERSIST_PENDING, STORAGE_PERSIST_FAILURE, SET_INSTALLED_STATUS } from './mutations';

const STORAGE_KEY = process.env.VUE_APP_STORAGE_KEY;
const CACHE_NAME  = process.env.VUE_APP_CACHE_NAME;

const STATUS_DEFAULT        = 'STATUS_DEFAULT';
const STATUS_OPENED         = 'STATUS_OPENED';
const STATUS_INSTALLED      = 'STATUS_INSTALLED';
const STATUS_CLAIMS_FETCHED = 'STATUS_CLAIMS_FETCHED';

const namespaced = true;

const state = {
  user: {
    uuid          : null,
    did           : null,
    url           : null,
    status        : STATUS_DEFAULT,
    persisting    : false,
    error         : null
  }
};

const getters = {
  uuid    : state => state.user.uuid,
  did     : state => state.user.did,
  url     : state => state.user.url,
  status  : state => state.user.status
};

const actions = {
  [MARK_AS_OPENED]: async ({ commit, dispatch }, {did, uuid, url}) => {
    try {
      commit(STORAGE_PERSIST_PENDING);
      const decodedUrl  = new Buffer(url, 'base64').toString('utf-8')
      const cache       = await caches.open(CACHE_NAME);

      cache.put(STORAGE_KEY, new Response(`{"uuid": "${uuid}", "did": "${did}", "url": "${decodedUrl}"}, "status": "${STATUS_OPENED}" `, {headers: {'Content-Type': 'application/json'}}));

      commit(STORAGE_PERSIST_SUCCESS, {did, uuid, url: decodedUrl});
    } catch (error) {
      commit(STORAGE_PERSIST_FAILURE, error.stack);
    }
  },

  [MARK_AS_INSTALLED]: async ({ commit, dispatch }) => {
    commit(SET_INSTALLED_STATUS);
  },
  [MARK_AS_INITIAL_CLAIMS_FETCHED]: async ({ commit, dispatch }) => {
    commit(SET_INSTALLED_STATUS, {status: 'STATUS_CLAIMS_FETCHED'});
  },
}

const mutations = {
  [STORAGE_PERSIST_PENDING](state) {
    state.user = { ...state.user, persisting: true };
  },
  [STORAGE_PERSIST_FAILURE](state, error) {
    state.user = {
      ...state.user,
      persisting: false,
      error
    }
  },
  [STORAGE_PERSIST_SUCCESS](state, {did, uuid, url}) {
    state.user = {
      ...state.user,
      status: STATUS_OPENED,
      did,
      uuid,
      url,
      error: null,
      persisting: false
    };
  },
  [SET_INSTALLED_STATUS](state, {status}) {
    state.user = {
      ...state.user,
      status: status || STATUS_INSTALLED
    };
  }
};

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
};
