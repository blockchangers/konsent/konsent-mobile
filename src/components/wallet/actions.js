export const CREATE_WALLET    = 'CREATE_WALLET';
export const CREATE_DID       = 'CREATE_DID';
export const SET_PHONE_NUMBER = 'SET_PHONE';
export const basePath         = type => `Wallet/${type}`;
