/* eslint-disable */

import * as did from '../../utils/did';
import * as wallet from '../../utils/wallet';

import { CREATE_DID, CREATE_WALLET, SET_PHONE_NUMBER } from './actions';
import {
  DID_FAILURE,
  DID_PENDING,
  DID_SUCCESS,
  WALLET_FAILURE,
  WALLET_PENDING,
  WALLET_PROGRESS,
  WALLET_SUCCESS,
  SET_PHONE
} from './mutations';

import web3 from '../../utils/web3';

const namespaced = true;

const state = {
  user: {
    wallet: null,
    did: null,
    creating: false,
    progress: 0,
    error: null,
    phone: ""
  },
};

const getters = {
  created: state => state.user.progress === 1 ? true : false,
  progress: state => state.user.progress * 100,
  wallet: state => state.user.wallet,
  did: state => state.user.did,
  isDidFromLocalStorage: state => !state.user.did.signJWT,
  isWalletNotReady: state => state.user.wallet && !state.user.did.signJWT,
  phone: state => state.user.phone
};

const actions = {
  [CREATE_WALLET]: async ({ commit, dispatch }, { seed, password }) => {
    try {
      commit(WALLET_PENDING);

      const result = await wallet.createEncrypted(seed, password, (progress) => {
        commit(WALLET_PROGRESS, progress);
      });

      commit(WALLET_SUCCESS, JSON.parse(result));

      dispatch(CREATE_DID, { password });
    } catch (error) {
      commit(WALLET_FAILURE, error.stack);
    }
  },
  [CREATE_DID]: async ({ commit, state }, { password }) => {
    return new Promise(async (resolve, reject) => {
      try {
        commit(DID_PENDING);
        const decryptedWallet = await wallet.load(JSON.stringify(state.user.wallet), password);
        const userDid = await did.create(web3, decryptedWallet);
        commit(DID_SUCCESS, userDid);
        return resolve(userDid);
      } catch (error) {
        commit(DID_FAILURE, error.stack);
        return reject(error.stack);
      }
    });
  },
  [SET_PHONE_NUMBER]: ({ commit }, { phone }) => {
    commit(SET_PHONE, phone)
  }
};

const mutations = {
  [WALLET_SUCCESS](state, wallet) {
    state.user = {
      ...state.user,
      wallet,
      error: null,
      progress: 1
    };
  },
  [WALLET_PROGRESS](state, progress) {
    state.user = {
      ...state.user,
      progress
    };
  },
  [WALLET_PENDING](state) {
    state.user = {
      ...state.user,
      creating: true
    };
  },
  [WALLET_FAILURE](state, error) {
    state.user = {
      ...state.user,
      creating: true,
      wallet: null,
      error
    }
  },
  [DID_SUCCESS](state, did) {
    state.user = {
      ...state.user,
      did,
      creating: false,
      error: null
    };
  },
  [DID_PENDING](state) {
    state.user = {
      ...state.user,
      creating: true
    };
  },
  [DID_FAILURE](state, error) {
    state.user = {
      ...state.user,
      creating: false,
      did: null,
      error
    }
  },
  [SET_PHONE](state, phone) {
    state.user = {
      ...state.user,
      phone
    }
  }
};

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations,
};
