export const WALLET_SUCCESS   = 'WALLET_SUCCESS';
export const WALLET_PENDING   = 'WALLET_PENDING';
export const WALLET_FAILURE   = 'WALLET_FAILURE';
export const WALLET_PROGRESS  = 'WALLET_PROGRESS';

export const DID_SUCCESS      = 'DID_SUCCESS';
export const DID_PENDING      = 'DID_PENDING';
export const DID_FAILURE      = 'DID_FAILURE';
export const DID_PROGRESS     = 'DID_PROGRESS';

export const SET_PHONE        = 'SET_PHONE';
