/* eslint-disable */

import { validateOtpInit, VALIDATE_OTP_INIT } from './actions';
import { VALIDATE_OTP_FAILURE, VALIDATE_OTP_PENDING, VALIDATE_OTP_SUCCESS } from './mutations';
import { SET_PHONE_NUMBER, basePath as walletStore } from '../wallet/actions';
import { post, validateToken } from '../../utils/ajax';

const KONSENT_OTP_VALIDATE_URI = process.env.VUE_APP_KONSENT_OTP_VALIDATE_URI;

const namespaced = true;

const state = {
  user: {
    token       : null,
    phone       : "",
    validated   : false,
    loading     : false,
    error       : null,
    jwt         : null
  }
};

const getters = {
  token       : state => state.user.token,
  phone       : state => state.user.phone,
  validated   : state => state.user.validated,
  loading     : state => state.user.loading,
  status      : state => state.user.error,
  error       : state => state.user.error,
  jwt         : state => state.user.jwt
};

const actions = {
  [VALIDATE_OTP_INIT]: async ({ commit, dispatch }, { token, phone }) => {
    return new Promise(async(resolve, reject) => {
      try {
        console.log("phone", phone)
        commit(VALIDATE_OTP_PENDING, phone);

        dispatch(walletStore(SET_PHONE_NUMBER), {phone}, {root: true})

        validateToken(KONSENT_OTP_VALIDATE_URI, { token, phone: phone.replace(/ /g,'') })
        .then(resp => {
          const {jwt} = resp;
          commit(VALIDATE_OTP_SUCCESS, {jwt});
          resolve(jwt);
        })
        .catch(error => {
          commit(VALIDATE_OTP_FAILURE, error);
          reject(error);
        });
      } catch (error) {
        reject(error);
      }
    });
  },
}

const mutations = {
  [VALIDATE_OTP_PENDING](state, phone) {
    state.user = {
      ...state.user,
      loading: true,
      phone
    };
  },
  [VALIDATE_OTP_FAILURE](state, error) {
    state.user = {
      ...state.user,
      loading: false,
      error
    }
  },
  [VALIDATE_OTP_SUCCESS](state, {jwt}) {
    state.user = {
      ...state.user,
      error: null,
      loading: false,
      validated: true,
      jwt
    };
  },
};

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
};
