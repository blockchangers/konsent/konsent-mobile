export const VALIDATE_OTP_INIT    = 'VALIDATE_OTP_INIT';
export const basePath           = type => `Otp/${type}`;
export const validateOtpInit = () => basePath(VALIDATE_OTP_INIT);
